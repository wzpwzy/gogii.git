package service

import (
	"gitee.com/wzpwzy/gogii/conf"
	"gitee.com/wzpwzy/gogii/tools"
)

/*
*创建Es service实例化方法
*
 */

func GenerateService(op conf.Options) {
	content := `package `
	if op.Namespace == "" {
		content += `elsa`
	} else {
		content += op.Namespace
	}

	if op.ModelsNamespace == "" {
		op.ModelsNamespace = "models"
	}

	content += `
import (
"context"
	model "` + op.ModelsNamespace + `"
	dao "` + op.DaoNamespace + `"
			
)
		
type ` + op.Name + `Service struct {
	es *dao.` + op.Name + `Es
}

func New` + op.Name + `Service(es *dao.` + op.Name + `Es) *` + op.Name + `Service {
		return &` + op.Name + `Service{
			es: es,
		}
}

func (s *` + op.Name + `Service) BatchAdd(ctx context.Context, ` + op.Name + ` []*model.` + op.Name + `) error {
	return s.es.BatchAdd(ctx, ` + op.Name + `)
}

func (s *` + op.Name + `Service) BatchDel(ctx context.Context, ` + op.Name + ` []*model.` + op.Name + `) error {
	return s.es.BatchDel(ctx, ` + op.Name + `)
}

func (s *` + op.Name + `Service) BatchUpdate(ctx context.Context, ` + op.Name + ` []*model.` + op.Name + `) error {
	return s.es.BatchUpdate(ctx, ` + op.Name + `)
}

func (s *` + op.Name + `Service) GetEs(ctx context.Context, IDS []uint64) ([]*model.` + op.Name + `, error) {
	return s.es.GetEs(ctx, IDS)
}

func (s *` + op.Name + `Service) Search(ctx context.Context, req *model.` + op.Name + `) ([]*model.` + op.Name + `, error) {
	return s.es.Search(ctx, req.ToFilter())
}

func (s *` + op.Name + `Service) Agg(ctx context.Context, req *model.` + op.Name + `) ([]byte, error) {
	return s.es.` + op.Name + `Agg(ctx, req.ToFilter())
}
	`
	filename := ""
	if op.Path == "" {
		filename = conf.ServicePath + op.Name + ".go"
	} else {
		filename = op.Path + op.Name + ".go"
	}
	tools.Write(filename, op.Name, content, conf.ServicePath)
}
