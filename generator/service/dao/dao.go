package dao

import (
	"gitee.com/wzpwzy/gogii/conf"
	"gitee.com/wzpwzy/gogii/tools"
	"strings"
)

// Case2Camel 下划线转驼峰(大驼峰)
func Case2Camel(name string) string {
	name = strings.Replace(name, "_", " ", -1) // 根据_来替换成
	name = strings.Title(name)                 // 全部大写
	return strings.Replace(name, " ", "", -1)  // 删除空格
}

func CreateDao(op conf.Options) {

	content := `package `
	if op.Namespace == "" {
		content += `dao`
	} else {
		content += op.Namespace
	}

	if op.ModelsNamespace == "" {
		op.ModelsNamespace = "models"
	}
	filename := op.Name
	if conf.MasterDbConfig.Prefix != "" {
		op.Name = strings.Replace(op.Name, conf.MasterDbConfig.Prefix, "", -1)
	}
	op.Name = Case2Camel(op.Name)
	content += `

import (
	"gorm.io/gorm"
	"` + op.ModelsNamespace + `"
)

type ` + op.Name + `Dao struct {
	db  *gorm.DB
}

func New` + op.Name + `(db *gorm.DB) *` + op.Name + `Dao {
	dao := &` + op.Name + `Dao{
		db:  db,
	}
	return dao
}

//添加
func (c *` + op.Name + `Dao) Create(v *model.` + op.Name + `)(id int, err error) {
	err = c.db.Create(&v).Error
	return v.ID,err
}

//修改
func (c *` + op.Name + `Dao) Update(v *model.` + op.Name + `)(id int, err error) {
	err = c.db.Save(&v).Error
	return v.ID,err
}

//删除
func (c *` + op.Name + `Dao) Delete(v *model.` + op.Name + `)(id int, err error) {
	err = c.db.Delete(&v).Error
	return v.ID,err
}



func (c *` + op.Name + `Dao) Get(where map[string]interface{},order string,pageSize int,page int) interface{} {
	var v []model.` + op.Name + `
	offset := (page - 1) * pageSize
	c.db.Where(where).Order(order).Limit(pageSize).Offset(offset).Find(&v)
	return v
}`

	if op.Path == "" {
		filename = conf.DaoPath + filename + ".go"
	} else {
		filename = op.Path + filename + ".go"
	}
	tools.Write(filename, op.Name, content, op.Path)
}
