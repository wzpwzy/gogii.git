package client

import (
	"fmt"
	"gitee.com/wzpwzy/gogii/conf"
	"github.com/olivere/elastic/v7"
	"log"
	"os"
	"time"
)

/*
*参数注解
*elastic.SetURL(url)用来设置ES服务地址，如果是本地，就是127.0.0.1:9200。支持多个地址，用逗号分隔即可。
*elastic.SetBasicAuth("user", "secret")这个是基于http base auth 验证机制的账号密码。
*elastic.SetGzip(true)启动gzip压缩
*elastic.SetHealthcheckInterval(10*time.Second)用来设置监控检查时间间隔
*elastic.SetMaxRetries(5)设置请求失败最大重试次数，v7版本以后已被弃用
*elastic.SetSniff(false)允许指定弹性是否应该定期检查集群（默认为true）
*elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)) 设置错误日志输出
*elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)) 设置info日志输出
 */

func NewEsClient(conf *conf.EsConfig) *elastic.Client {
	url := fmt.Sprintf("http://%s:%d", conf.Host, conf.Port)
	client, err := elastic.NewClient(
		//elastic 服务地址
		elastic.SetURL(url),
		//是否开启集群嗅探
		elastic.SetSniff(false),
		//设置验证
		//elastic.SetBasicAuth(conf.Username, conf.Password),
		//启动压缩
		elastic.SetGzip(true),
		//用来设置监控检查时间间隔
		elastic.SetHealthcheckInterval(10*time.Second),
		// 设置错误日志输出
		elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)),
		// 设置info日志输出
		elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)))
	if err != nil {
		log.Fatalln("Failed to create elastic client")
	}
	return client
}

type EsSearch struct {
	MustQuery    []elastic.Query
	MustNotQuery []elastic.Query
	ShouldQuery  []elastic.Query
	Filters      []elastic.Query
	Sorters      []elastic.Sorter
	Aggregation  elastic.Aggregation
	From         int
	Size         int
}
