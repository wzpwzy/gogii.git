package conf

// model保存路径
const ModelPath = "./models/"

// dao 文件路径
const DaoPath = "./service/dao/"

// daoES 文件路径
const EsDaoPath = "./service/elastic/"

// Service 文件路径
const ServicePath = "./service/"

// 是否覆盖已存在model
const ModelReplace = true

// 数据库驱动
const DriverName = "mysql"

type DbConf struct {
	Host   string
	Port   string
	User   string
	Pwd    string
	DbName string
	Url    string
	Prefix string
}

// 数据库链接配置
var MasterDbConfig DbConf = DbConf{
	Host:   "127.0.0.1",
	Port:   "3306",
	User:   "root",
	Pwd:    "",
	Prefix: "",
	DbName: "rageframe",
	Url:    "",
}

/*
*ElasticConfig ES配置
 */

type EsConfig struct {
	Host     string
	Port     int
	Database int
	Username string
	Password string
	Url      string
}

var ElasticConfig EsConfig = EsConfig{
	Host:     "127.0.0.1",
	Port:     9200,
	Database: 0,
	Username: "",
	Password: "",
	Url:      "",
}

type Options struct {
	Name            string
	Path            string
	Packet          map[string]string
	Namespace       string
	ModelsNamespace string
	DaoNamespace    string
	FileName        string
}
