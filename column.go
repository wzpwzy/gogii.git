package gogii

import (
	"fmt"
	"gitee.com/wzpwzy/gogii/conf"
	"gitee.com/wzpwzy/gogii/generator/model"
	"gitee.com/wzpwzy/gogii/generator/service/dao"
	"gitee.com/wzpwzy/gogii/tools"
	_ "github.com/go-sql-driver/mysql"
)

//Column
/**
*一键生成所有文件
 */
func Column(soure string, table string, path string) {
	//初始化配置
	var MysqlConfigDbConf = conf.DbConf{
		DbName: "rageframe",
		Url:    soure,
	}
	conf.MasterDbConfig = MysqlConfigDbConf
	//初始化Mysql连接池
	mysql := tools.GetMysqlInstance().InitMysqlPool()
	if !mysql {
		fmt.Println("init database pool failure...")
	}
	var options conf.Options
	var modelList []model.Table
	modelList = model.Genertate(path, 1, table) //生成所有表信息
	for _, model := range modelList {
		options.Namespace = "dao"
		options.Name = model.Name
		options.Path = "/service/dao"
		dao.CreateDao(options)
	}

}
