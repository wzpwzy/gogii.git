package gogii

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

/**
*fmt.Scanln(&table) //方法-
*fmt.Scanf("%d %s %f %t",&age,&name,&score,&isVIP)
 */
func main() {
	//实现功能：代码生成器
	//方式1：Scanln
	var table string
	var path string
	//录入数据的时候，类型一定要匹配，因为底层会自动判定类型的
	fmt.Scanln(&table)
	//数据库配置
	source := "root:@tcp(127.0.0.1:3306)/rageframe?charset=utf8&parseTime=True&loc=Local"
	Column(source, table, path)
	//beego.Run()
}
